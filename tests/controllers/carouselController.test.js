const supertest = require('supertest');
const app = require('../../src/app');

describe('GET /widgets', () => {
  test('returns error when no template is provided', async () => {
    const res = await supertest(app).get('/widgets?company_name=Hippopotamus Nantes');
    expect(res.body).toEqual('No `template` found in query params.');
  });

  test('returns error for unavailable templates', async () => {
    const res = await supertest(app).get('/widgets?template=unavailable&company_name=Hippopotamus Nantes');
    expect(res.body.data).toContain('The request template isn\'t available');
  });

  test('returns error when no company_name is provided', async () => {
    const res = await supertest(app).get('/widgets?template=badge');
    expect(res.body).toEqual('No `company_name` found in query params.');
  });

  test(`returns badge data when badge template is requested`, async () => {
    const res = await supertest(app).get('/widgets?template=badge&company_name=Hippopotamus Nantes');
    expect(res.headers['content-type']).toContain('text/html');
    expect(res.text).toContain('yellow_star.png');
  });

  test('returns carousel data when carousel template is requested', async () => {
    const res = await supertest(app).get('/widgets?template=carousel&company_name=Hippopotamus Nantes');
    expect(res.headers['content-type']).toContain('text/html');
    expect(res.text).toContain('carouselCardContainer');
  });
});
