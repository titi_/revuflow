const supertest = require('supertest');
const app = require('../../src/app');

describe('CORS webflow.com',
    () => {
      test('should allow requests from webflow.com', async () => {
        const allowedOrigin = 'https://webflow.com';

        const response = await supertest(app)
            .get('/widgets?template=carousel&company_name=Hippopotamus Nantes')
            .set('Origin', allowedOrigin);

        expect(response.headers['access-control-allow-origin']).toEqual(allowedOrigin);
      });
    });
describe('CORS localhost',
    () => {
      test('should allow requests from localhost', async () => {
        const allowedOrigin = 'http://localhost';

        const response = await supertest(app)
            .get('/widgets?template=carousel&company_name=Hippopotamus Nantes')
            .set('Origin', allowedOrigin);

        expect(response.headers['access-control-allow-origin']).toEqual(allowedOrigin);
      });
    });
describe('CORS weird URL',
    () => {
      test('should not allow requests from unallowed domains', async () => {
        const allowedOrigin = 'https://bettercallsaul.com';

        const response = await supertest(app)
            .get('/widgets?template=carousel&company_name=Hippopotamus Nantes')
            .set('Origin', allowedOrigin);

        expect(response.headers['access-control-allow-origin']).toBeUndefined();
      });
    });
