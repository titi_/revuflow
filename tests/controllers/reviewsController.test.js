const supertest = require('supertest');
const app = require('../../src/app');

describe('GET /reviews?companyName=Hippopotamus Nantes',
    () => {
      test('will return the name and rating of a company', async () => {
        const res = await supertest(app)
            .get('/reviews?companyName=Hippopotamus Nantes');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(res.body.data).toHaveProperty('name');
        expect(typeof res.body.data.name).toBe('string');
        expect(res.body.data).toHaveProperty('rating');
        expect(typeof res.body.data.rating).toBe('number');
      });
    });

describe('GET /reviews?companyName=Hippopotamus Nantes&includes=reviews',
    () => {
      test('will return the name and rating of a company with its last reviews',
          async () => {
            const res = await supertest(app)
                .get('/reviews?companyName=Hippopotamus Nantes&includes=reviews');

            console.log(res.body);
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('data');
            expect(res.body.data).toHaveProperty('name');
            expect(typeof res.body.data.name).toBe('string');
            expect(res.body.data).toHaveProperty('rating');
            expect(typeof res.body.data.rating).toBe('number');
            expect(res.body.data).toHaveProperty('reviews');
            expect(Array.isArray(res.body.data.reviews)).toBeTruthy();
          });
    });

describe('GET /reviews?companyName=einoierhziughefigchkczeè§"rèhseozritghizeohyoiezrhyoerutçyertoyifkreuyjçeyiognçzez§eçreoizhnyieru',
    () => {
      test('will return empty data',
          async () => {
            const res = await supertest(app)
                .get('/reviews?companyName=einoierhziughefigchkczeè§"rèhseozritghizeohyoiezrhyoerutçyertoyifkreuyjçeyiognçzez§eçreoizhnyieru');

            console.log(res.body);
            expect(res.statusCode).toBe(200);
            expect(res.body).toHaveProperty('data');
            expect(typeof res.body.data).toBe('object');
            expect(res.body.data).toEqual({});
          });
    });

describe('GET /reviews',
    () => {
      test('will return an error saying that parameter companyName must be provided',
          async () => {
            const res = await supertest(app).get('/reviews');

            console.log(res.body);
            expect(res.statusCode).toBe(500);
            expect(res.body).toHaveProperty('data');
            expect(typeof res.body.data).toBe('string');
            expect(res.body.data).toEqual('`companyName` query parameter must be provided');
          });
    });

describe('GET /reviews?companyName=Hippopotamus%20Nantes',
    () => {
      let originalApiKey;

      beforeAll(() => {
        originalApiKey = process.env.API_KEY;
      });

      afterAll(() => {
        process.env.API_KEY = originalApiKey;
      });
      test('will return an error saying that no API key is set',
          async () => {
            delete process.env.API_KEY;
            const res = await supertest(app).get('/reviews?companyName=Hippopotamus%20Nantes');

            console.log(res.body);
            expect(res.statusCode).toBe(500);
            expect(res.body).toHaveProperty('data');
            console.log(res.body.data);
            expect(typeof res.body.data).toBe('string');
            expect(res.body.data).toEqual('API_KEY is not defined in the environment variables.');
          });
    });
