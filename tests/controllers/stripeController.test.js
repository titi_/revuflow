const supertest = require('supertest');
const app = require('../../src/app');

describe('GET /stripe/has-access?email=hello-world.com',
    () => {
      test('will return an error as the email validator didn\'t pass', async () => {
        const res = await supertest(app)
            .get('/stripe/has-access?email=hello-world.com');

        console.log(res.body);
        expect(res.statusCode).toBe(500);
        expect(res.body).toHaveProperty('data');
        console.log(res.body.data);
        expect(typeof res.body.data).toBe('string');
        expect(res.body.data).toEqual('Invalid email format');
      });
    });

describe('GET /stripe/has-access?email=hello-world@gmail.com',
    () => {
      test('will return an error as the email validator didn\'t pass', async () => {
        const res = await supertest(app)
            .get('/stripe/has-access?email=hello-world@gmail.com');

        console.log(res.body);
        expect(res.statusCode).toBe(500);
        expect(res.body).toHaveProperty('data');
        console.log(res.body.data);
        expect(typeof res.body.data).toBe('string');
        expect(res.body.data).toEqual('`websiteId` query parameter must be provided');
      });
    });

describe('GET /stripe/has-access?email=hello-world@mb.com&websiteId=1ER23YTFZ',
    () => {
      test('will return that the email hasn\'t any subscription', async () => {
        const res = await supertest(app)
            .get('/stripe/has-access?email=hello-world@mb.com&websiteId=1ER23YTFZ');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        console.log(res.body.data);
        expect(typeof res.body.data).toBe('boolean');
        expect(res.body.data).toEqual(false);
      });
    });

describe('GET /stripe/has-access?email=tibo.d37@gmail.com&websiteId=63b7174545c68869844cf11e',
    () => {
      test('will return that the email has a subscription', async () => {
        const res = await supertest(app)
            .get('/stripe/has-access?email=tibo.d37@gmail.com&websiteId=63b7174545c68869844cf11e');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('boolean');
        expect(res.body.data).toEqual(true);
      });
    });

describe('GET /stripe/customers',
    () => {
      test('will return an array of users', async () => {
        const res = await supertest(app)
            .get('/stripe/customers');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('object');
        expect(res.body.data).toHaveProperty('data');
        expect(typeof res.body.data.data).toBe('object');
      });
    });

describe('GET /stripe/customers/tibo.d37@gmail.com',
    () => {
      test('will return the user for the given email address', async () => {
        const res = await supertest(app)
            .get('/stripe/customers/tibo.d37@gmail.com');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('object');
        expect(res.body.data).toHaveProperty('id');
      });
    });

describe('GET /stripe/customers/tibo.d69@gmal.com',
    () => {
      test('will return the user for the given email address', async () => {
        const res = await supertest(app)
            .get('/stripe/customers/tibo.d69@gmal.com');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('object');
        expect(JSON.stringify(res.body.data)).toBe(JSON.stringify({}));
      });
    });

describe('PUT /stripe/customers/tibo.d37@gmail.com',
    () => {
      test('will update the user\'s metadata with the given website_id', async () => {
        const res = await supertest(app)
            .put('/stripe/customers/tibo.d37@gmail.com')
            .type('form')
            .send({
              field_to_update: 'metadata',
              website_id: 1234,
            });

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('object');
        expect(res.body.data).toHaveProperty('id');
        await supertest(app)
            .put('/stripe/customers/tibo.d37@gmail.com')
            .type('form')
            .send({
              field_to_update: 'metadata',
              website_id: '63b7174545c68869844cf11e',
            });
      });
    });

describe('PUT /stripe/customers/tibo.d37@gmai.com',
    () => {
      test('will return an empty array as this email does not exist', async () => {
        const res = await supertest(app)
            .put('/stripe/customers/tibo.d37@gmai.com')
            .type('form')
            .send({
              field_to_update: 'metadata',
              website_id: 1234,
            });

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('object');
        expect(JSON.stringify(res.body.data)).toBe(JSON.stringify([]));
      });
    });

describe('PUT /stripe/customers/tibo.d37@gmail.com',
    () => {
      test('will return an empty array as this email does not exist', async () => {
        const res = await supertest(app)
            .put('/stripe/customers/tibo.d37@gmai.com')
            .type('form')
            .send({
              field_to_update: 'metadatum',
              website_id: 1234,
            });

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(typeof res.body).toBe('string');
        expect(res.body).toEqual('Available fields to update : \'metadata\'.');
      });
    });

describe('PUT /stripe/customers/tibo.d37@gmail.com',
    () => {
      test('will return an empty array as this email does not exist', async () => {
        const res = await supertest(app)
            .put('/stripe/customers/tibo.d37@gmail.com')
            .type('form')
            .send({
              field_to_update: 'metadata',
            });

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(typeof res.body).toBe('string');
        expect(res.body).toEqual('No `website_id` found in request body.');
      });
    });

describe('GET /stripe/has-subscription?email=tibo.d37@gmail.com',
    () => {
      test('will return a true boolean as the user does have a subscription', async () => {
        const res = await supertest(app)
            .get('/stripe/has-subscription?email=tibo.d37@gmail.com');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('boolean');
        expect(res.body.data).toBe(true);
      });
    });

describe('GET /stripe/has-subscription?email=tibo.d37@gmail.co',
    () => {
      test('will return a false boolean as the user does not have a subscription', async () => {
        const res = await supertest(app)
            .get('/stripe/has-subscription?email=tibo.d37@gmail.co');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('boolean');
        expect(res.body.data).toBe(false);
      });
    });

describe('GET /stripe/has-subscription',
    () => {
      test('will return that the email was not provided', async () => {
        const res = await supertest(app)
            .get('/stripe/has-subscription');

        console.log(res.body);
        expect(res.statusCode).toBe(500);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('string');
        expect(res.body.data).toBe('`email` query parameter must be provided');
      });
    });

describe('GET /stripe/has-metadata',
    () => {
      test('will return that the email was not provided', async () => {
        const res = await supertest(app)
            .get('/stripe/has-metadata');

        console.log(res.body);
        expect(res.statusCode).toBe(500);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('string');
        expect(res.body.data).toBe('`email` query parameter must be provided');
      });
    });

describe('GET /stripe/has-metadata?email=tibo.d37@gmail.co',
    () => {
      test('will return a false boolean as the user does not have a website ID in its metadata', async () => {
        const res = await supertest(app)
            .get('/stripe/has-metadata?email=tibo.d37@gmail.co');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('boolean');
        expect(res.body.data).toBe(false);
      });
    });

describe('GET /stripe/has-metadata?email=tibo.d37@gmail.com',
    () => {
      test('will return a true boolean as the user does have a website ID in its metadata', async () => {
        const res = await supertest(app)
            .get('/stripe/has-metadata?email=tibo.d37@gmail.com');

        console.log(res.body);
        expect(res.statusCode).toBe(200);
        expect(res.body).toHaveProperty('data');
        expect(typeof res.body.data).toBe('boolean');
        expect(res.body.data).toBe(true);
      });
    });
