# revuflow

[![coverage report](https://gitlab.com/titi_/revuflow/badges/main/coverage.svg)](https://gitlab.com/titi_/revuflow/-/commits/main/coverage/lcov.info)
[![pipeline status](https://gitlab.com/titi_/revuflow/badges/main/pipeline.svg)](https://gitlab.com/titi_/revuflow/-/commits/main)

## Getting started

To run the API : 

```
cd revuflow
npm i
node server.js
```