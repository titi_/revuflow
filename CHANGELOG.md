# Changelog

All notable changes to this project will be documented in this file.

The format is based on [Keep a Changelog](https://keepachangelog.com/en/1.0.0/),
and this project adheres to [Semantic Versioning](https://semver.org/spec/v2.0.0.html).

## [Unreleased]

## [2.0.0] - 2023-09-28
### Added
- Route `/stripe/get-subscription-status` to get the status of a Stripe subscription using the given e-mail
### Changed
- Removed `/api/` from endpoints. The app is now accessible from [https://api.revuflow.net](https://api.revuflow.net).

## [1.0.0] - 2023-09-13
### Added
- The global coverage must be > 85%.
- Gitlab CI that runs :
  - tests with Jest & Supertest
  - coverage with Ratchet & Jest
  - ESLint
### Changed
- Indentation (4 spaces to 2)

## [0.1.0] - 2023-09-12
### Added
- [Redis](https://redis.io/) caching system. The cache expires every 30 minutes.
- Winston logging system.
### Changed
- Separated controllers, middlewares and configurations.
- Refactored main file to match SoC.

## [0.0.1] - 2023-09-11
### Added
- API's first version.
- Initial documentation with OpenAPI (see `/docs` route).