const cors = require('cors');

allowedDomains = [
  'https://webflow.com',
  'https://6516d453b1c4f44363996b82.webflow-ext.com',
];

const corsOptions = {
  origin: function(origin, callback) {
    if (!origin) return callback(null, true);
    if (allowedDomains.indexOf(origin) !== -1 || (origin && origin.startsWith('http://localhost'))) {
      callback(null, true);
    } else {
      callback(new Error('Not allowed by CORS'));
    }
  },
  methods: ['GET', 'PUT'],
  credentials: true,
  optionsSuccessStatus: 200,
};

module.exports = cors(corsOptions);
