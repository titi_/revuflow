const logger = require('../config/winston');

module.exports = (err, req, res, next) => {
  logger.error(err.message, {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  const statusCode = err.statusCode || 500;

  res.status(statusCode).json({data: err.message});
};
