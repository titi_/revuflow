const swaggerJsdoc = require('swagger-jsdoc');

const options = {
  definition: {
    openapi: '3.1.0',
    info: {
      title: 'RevuFlow API',
      description: 'API Interface for the Google Business reviews',
      version: '1.0.0',
    },
  },
  apis: ['./src/app.js', './src/controllers/reviewsController.js'],
};

module.exports = swaggerJsdoc(options);
