require('dotenv').config();

module.exports = {
  port: 8080,
  textSearchUrl: 'https://maps.googleapis.com/maps/api/place/textsearch/json',
  detailsUrl: 'https://maps.googleapis.com/maps/api/place/details/json',
  allowedDomains: ['https://your-webflow-domain.com'],
};
