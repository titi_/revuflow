const logger = require('../config/winston');
const Reviews = require('../utils/Reviews');

/**
 * @openapi
 * /reviews:
 *   get:
 *     tags:
 *       - reviews
 *     summary: Get the rating and reviews of a company using the Google Places API
 *     parameters:
 *       - in: query
 *         name: companyName
 *         schema:
 *           type: string
 *         required: true
 *         description: Name of the company to search for
 *       - in: query
 *         name: includes
 *         schema:
 *           type: array
 *           items:
 *             type: string
 *             enum: [reviews]
 *         style: form
 *         explode: false
 *         description: Add details, like the company's reviews
 *     responses:
 *       200:
 *         description: Rating and reviews if included
 *         content:
 *           application/json:
 *             examples:
 *               rating:
 *                 summary: Rating
 *                 value: {data: {companyName: Hippopotamus Nantes, rating: 3.9}}
 *               ratingandreviews:
 *                 summary: RatingAndReviews
 *                 value: {data: {companyName: Hippopotamus Nantes, rating: 3.9, reviews: [{"author_name": "Marc Blake","author_url": "https://www.google.com/maps/contrib/107802927388325232520/reviews",
 *                   "language": "en","original_language": "en","profile_photo_url": "https://lh3.googleusercontent.com/a-/ALV-UjWZRiL4bPHCrnPXLjzRQ50rippE-0O48fIp38dVZSkhfw=s128-c0x00000000-cc-rp-mo-ba6",
 *                   "rating": 5,"relative_time_description": "10 months ago","text": "The only place open in this area on Sunday night. Enjoy beer on tap, complimentary homemade chips and country salad.",
 *                   "time": 1667933526,"translated": false},]}}
 *       500:
 *         description: Server error
 *         content:
 *           application/json:
 *             schema:
 *               type: string
 *               example: {data: "Company 'Hippopotamus Nantes' not found"}
 */
exports.getReviews = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    const companyName = req.query.companyName;

    if (!companyName) {
      throw new Error('`companyName` query parameter must be provided');
    }

    const data = await Reviews.getForCompany(companyName);

    res.json(data);
  } catch (error) {
    next(error);
  }
};
