const logger = require('../config/winston');
const AVAILABLE_TEMPLATES = ['badge', 'carousel'];
const Reviews = require('../utils/Reviews');
const maxLength = 200;

exports.getWidgets = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });

  if (!req.query.template) {
    return res.json('No `template` found in query params.');
  }

  if (!AVAILABLE_TEMPLATES.includes(req.query.template)) {
    return res.json({data: `The request template isn\'t available. The available templates are : ${AVAILABLE_TEMPLATES.join(', ')}.`});
  }

  if (!req.query.company_name) {
    return res.json('No `company_name` found in query params.');
  }

  Reviews.getForCompany(req.query.company_name).then(({data}) => {
    let content = {};
    switch (req.query.template) {
      case 'badge':
        content = {
          rating: data.rating,
          url: data.url,
          stars: getStars(data.rating),
        };
        break;
      case 'carousel':
        content = {
          rating: data.rating,
          url: data.url,
          stars: getStars(data.rating),
          reviews: getFormattedCarouselData(data.reviews),
        };
        break;
    }

    res.render(req.query.template, content);
  });
};

/**
 * @param rating
 * @returns {{color: string}[]}
 */
function getStars(rating) {
  const roundedRating = Math.round(rating);

  return Array.from({length: 5}, (_, index) => ({
    color: index < roundedRating ? 'yellow' : 'grey',
  }));
}

/**
 * @param reviews
 * @returns {*}
 */
function getFormattedCarouselData(reviews) {
  return reviews.map((review) => ({
    ...review,
    description: review.text.length > maxLength ?
        review.text.substring(0, maxLength) + '...' :
        review.text,
    stars: getStars(review.rating),
  }));
}
