const stripe = require('stripe')(process.env.STRIPE_API_KEY);
const logger = require('../config/winston');
const cache = new Map();
const emailRegex = /^[a-zA-Z0-9._%+-]+@[a-zA-Z0-9.-]+\.[a-zA-Z]{2,}$/;

exports.getUsers = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    const limit = req.query.limit ?? 10;
    const result = await stripe.customers.list({
      limit: Math.min(limit, 50),
    });

    logger.info('Responding with users list', {limit: limit, data: result});
    return res.json({
      data: result,
    });
  } catch (error) {
    next(error);
  }
};

exports.getUserByEmail = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    const userEmail = req.params.email;

    checkEmail(userEmail);

    const customers = await stripe.customers.search({
      query: `email:'${userEmail}'`,
    });

    customers.data.length > 0 ?
        logger.info('Responding with user', {data: customers.data[0]}) :
        logger.info('Responding with an empty response', {});

    return res.json({
      data: customers.data.length > 0 ? customers.data[0] : {},
    });
  } catch (error) {
    next(error);
  }
};

exports.updateCustomer = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    if (req.body.field_to_update !== 'metadata') {
      return res.json('Available fields to update : \'metadata\'.');
    }

    if (!req.body.website_id) {
      return res.json('No `website_id` found in request body.');
    }

    const customers = await stripe.customers.search({
      query: `email:\'${req.params.email}\'`,
    });

    // TODO return a 400 code
    if (customers.data.length === 0) {
      logger.info('Responding with empty array as no user has been found', {});

      return res.json({
        data: [],
      });
    }

    const customer = await stripe.customers.update(
        customers.data[0].id,
        {metadata: {website_id: req.body.website_id}},
    );

    logger.info('Responding with updated user', {data: customer});

    return res.json({
      data: customer,
    });
  } catch (error) {
    next(error);
  }
};

/**
  * Retrieves the website ID associated with a given e-mail.
  *
  * This function first checks whether the website ID is cached.
  * If so, it returns the ID from the cache.
  * If not, it performs a customer search in Stripe using the e-mail provided.
  * The website ID is then cached and returned.
  *
  * @async
  * @param {string} email - The email address for which to search for a website ID.
  * @returns {Promise<string|null>} The website ID associated with the given email or `null` if no ID is found.
  * @throws {Error} An error is thrown if the Stripe search fails.
 */
async function getWebsiteId(email) {
  const cachedWebsiteId = cache.get(email);

  if (cachedWebsiteId) return Promise.resolve(cachedWebsiteId);

  return stripe.customers.search({
    query: `email:\'${email}\'`,
  }).then((customers) => {
    cache.set(email, customers.data[0]?.metadata.website_id);
    return customers.data[0]?.metadata.website_id;
  });
}

exports.hasAccess = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    const userEmail = req.query.email;
    const reqWebsiteId = req.query.websiteId;

    checkEmail(userEmail);

    if (!reqWebsiteId) {
      throw new Error('`websiteId` query parameter must be provided');
    }

    return getWebsiteId(userEmail).then((stripeWebsiteId) => {
      logger.info('Responding with a boolean', {data: stripeWebsiteId === reqWebsiteId});

      return res.json({
        data: stripeWebsiteId === reqWebsiteId,
      });
    });
  } catch (error) {
    next(error);
  }
};

exports.hasSubscription = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    const userEmail = req.query.email;

    checkEmail(userEmail);

    const customers = await stripe.customers.search({
      query: `email:'${userEmail}'`,
    });

    customers.data.length > 0 ?
      logger.info('Responding with a true boolean', {data: customers.data.length > 0}) :
      logger.info('Responding with a false boolean', false);

    return res.json({
      data: customers.data.length > 0,
    });
  } catch (error) {
    next(error);
  }
};


exports.hasMetadata = async (req, res, next) => {
  logger.info('Received request', {
    method: req.method,
    url: req.url,
    user_agent: req.headers['user-agent'],
    query_params: req.query,
  });
  try {
    const userEmail = req.query.email;

    checkEmail(userEmail);

    const customers = await stripe.customers.search({
      query: `email:'${userEmail}'`,
    });

    customers.data.length > 0 ?
      logger.info('Responding with a true boolean', {data: customers.data[0].metadata.website_id !== undefined}) :
      logger.info('Responding with a false boolean', false);

    return res.json({
      data: customers.data.length > 0 ? customers.data[0].metadata.website_id !== undefined : false,
    });
  } catch (error) {
    next(error);
  }
};

// eslint-disable-next-line require-jsdoc
function checkEmail(email) {
  if (!email) {
    throw new Error('`email` query parameter must be provided');
  }

  if (!emailRegex.test(email)) {
    throw new Error('Invalid email format');
  }
}
