const express = require('express');
const corsMiddleware = require('./middleware/cors');
const errorHandler = require('./middleware/errorHandler');
const routes = require('./routes');
const swaggerUi = require('swagger-ui-express');
const specs = require('./config/swagger');
const bodyParser = require('body-parser');
const mustacheExpress = require('mustache-express');

const app = express();

app.use(corsMiddleware);

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({extended: true}));

app.use(routes);

app.use('/docs', swaggerUi.serve, swaggerUi.setup(specs));

app.use(errorHandler);

app.set('views', `${__dirname}/../views`);
app.set('view engine', 'mustache');
app.engine('mustache', mustacheExpress());

module.exports = app;
