const config = require('../config/variables');

Reviews = {
  async getForCompany(companyName) {
    const apiKey = process.env.API_KEY;

    if (!apiKey) {
      throw new Error('API_KEY is not defined in the environment variables.');
    }

    const qp = {
      query: companyName,
      key: apiKey,
    };

    const qs = new URLSearchParams(qp).toString();

    const r = await fetch(`${config.textSearchUrl}?${qs}`);
    const companies = (await r.json())?.results;

    if (!companies[0]) {
      return {data: {}};
    }

    const queryParams = {
      place_id: companies[0].place_id,
      key: process.env.API_KEY,
      fields: 'reviews,url',
      reviews_sort: 'newest',
      reviews_no_translations: true,
    };

    const queryString = new URLSearchParams(queryParams).toString();

    return fetch(`${config.detailsUrl}?${queryString}`)
        .then((response) => response.json())
        .then((data) => {
          return {
            data: {
              name: companies[0].name,
              rating: companies[0].rating,
              reviews: data.result.reviews,
              url: data.result.url,
            },
          };
        });
  },
};

module.exports = Reviews;
