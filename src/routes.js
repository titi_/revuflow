const express = require('express');
const router = express.Router();
const {getReviews} = require('./controllers/reviewsController');
const {getWidgets} = require('./controllers/widgetsController');
const {getUsers, updateCustomer, hasAccess, getUserByEmail, hasSubscription, hasMetadata} = require('./controllers/stripeController');
const redis = require('redis');
const client = redis.createClient();
const expressRedisCache = require('express-redis-cache')({
  client: client,
  expire: 1800,
});

router.get('/reviews', expressRedisCache.route(), getReviews);

router.get('/stripe/customers', expressRedisCache.route(), getUsers);

router.get('/stripe/customers/:email', expressRedisCache.route(), getUserByEmail);

router.get('/stripe/has-access', expressRedisCache.route(), hasAccess);

router.put('/stripe/customers/:email', expressRedisCache.route(), updateCustomer);

router.get('/stripe/has-subscription', expressRedisCache.route(), hasSubscription);

router.get('/stripe/has-metadata', expressRedisCache.route(), hasMetadata);

router.get('/widgets', expressRedisCache.route(), getWidgets);

module.exports = router;
