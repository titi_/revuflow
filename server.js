const app = require('./src/app');
const config = require('./src/config/variables');
const logger = require('./src/config/winston');

app.listen(config.port, () => {
  logger.info(`Server started on port ${config.port}`);
});
